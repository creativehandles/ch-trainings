<?php

namespace Creativehandles\ChTrainings;

use Creativehandles\ChTrainings\Console\BuildTrainingsPackageCommand;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\ServiceProvider;

class ChTrainingsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'ch-trainings');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'ch-trainings');
         $this->loadMigrationsFrom(__DIR__.'/../app/Plugins/Trainings/Migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');

        if ($this->app->runningInConsole()) {

            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('ch-trainings.php'),
            ], 'config');


            // Publishing the blog controller.
            $this->publishes([
                __DIR__.'/../app/Http/Controllers/' => app_path('/Http/Controllers/'),
            ], 'pluginController');


            //publishing routes and breadcrumbs
            $this->publishes([
                __DIR__.'/../routes' => base_path('routes/packages'),
            ], 'routes');

            // Publishing the views.
            $this->publishes([
                __DIR__.'/../resources/views' => resource_path('views/Admin/'),
            ], 'views');

            // Registering package commands.
            $this->commands([
                BuildTrainingsPackageCommand::class
            ]);
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'ch-trainings');

        // Register the main class to use with the facade
        $this->app->singleton('ch-trainings', function () {
            return new ChTrainings;
        });
    }
}
