<?php
/**
 * Created by PhpStorm.
 * User: deemantha
 * Date: 2/7/19
 * Time: 7:14 PM
 */

namespace Creativehandles\ChTrainings\Plugins\Trainings\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class LowTrainingCategoryModel extends Model
{

    protected $table = 'low_trainings_category';
    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];

    protected $guarded = [];

    use SoftDeletes;

    protected static function boot()
    {
        parent::boot(); // TODO: Change the autogenerated stub
        // extend deleting function
        static::deleting(function ($model) {
            // keep who deletes the record
            $model->deleted_by = Auth::id();
            $model->save();
        });
    }

}