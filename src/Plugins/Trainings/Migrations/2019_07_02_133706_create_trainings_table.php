<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainings', function (Blueprint $table) {
            $table->increments('trainings_id');
            $table->string("trainings_title");
            $table->string("trainings_subtitle");
            $table->integer("trainings_category_id")->default(1);
            $table->string("trainings_feature_type")->nullable()->default('img');
            $table->text("trainings_feature_video")->nullable();
            $table->text("trainings_featured_image")->nullable();
            $table->longText("trainings_body1")->nullable();
            $table->longText("trainings_body2")->nullable();
            $table->decimal("trainings_price_wo_vat")->nullable();
            $table->decimal("trainings_price_with_vat")->nullable();
            $table->string("trainings_video_count")->nullable();
            $table->string("trainings_document")->nullable();
            $table->string("trainings_video_length")->nullable();
            $table->string("trainings_benefit")->nullable();
            $table->string("trainings_course_duration")->nullable();
            $table->string("trainings_instructor")->nullable();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainings');
    }
}
